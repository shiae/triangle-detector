﻿using System;

namespace TriangleDetector
{
    public class TriangleTypeDetector
    {
        private readonly double sideA;
        private readonly double sideB;
        private readonly double sideC;

        public TriangleTypeDetector(double sideA, double sideB, double sideC)
        {
            this.sideA = sideA;
            this.sideB = sideB;
            this.sideC = sideC;
        }

        public string DetermineTriangleType()
        {
            if (IsValidTriangle())
            {
                if (IsRightTriangle())
                {
                    return "Прямоугольный треугольник";
                }
                else if (IsObtuseTriangle())
                {
                    return "Тупоугольный треугольник";
                }
                else
                {
                    return "Остроугольный треугольник";
                }
            }
            else
            {
                return "Неверные значения сторон";
            }
        }

        private bool IsValidTriangle()
        {
            return sideA + sideB > sideC && sideA + sideC > sideB && sideB + sideC > sideA &&
                   sideA > 0 && sideB > 0 && sideC > 0;
        }

        private bool IsRightTriangle()
        {
            return (sideA * sideA == sideB * sideB + sideC * sideC) ||
                   (sideB * sideB == sideA * sideA + sideC * sideC) ||
                   (sideC * sideC == sideA * sideA + sideB * sideB);
        }

        private bool IsObtuseTriangle()
        {
            return (sideA * sideA > sideB * sideB + sideC * sideC) ||
                   (sideB * sideB > sideA * sideA + sideC * sideC) ||
                   (sideC * sideC > sideA * sideA + sideB * sideB);
        }
    }
}
