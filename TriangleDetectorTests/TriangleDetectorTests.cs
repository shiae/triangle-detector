﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleDetector;

namespace TriangleDetectorTests
{
    [TestClass]
    public class TriangleDetectorTests
    {
        [TestMethod]
        public void DetermineTriangleType_RightTriangle_ReturnsRightTriangle()
        {
            double sideA = 4;
            double sideB = 3;
            double sideC = 5;
            TriangleTypeDetector detector = new TriangleTypeDetector(sideA, sideB, sideC);

            string result = detector.DetermineTriangleType();

            Assert.AreEqual("Прямоугольный треугольник", result);
        }

        [TestMethod]
        public void DetermineTriangleType_ObtuseTriangle_ReturnsObtuseTriangle()
        {
            double sideA = 5;
            double sideB = 18;
            double sideC = 15;
            TriangleTypeDetector detector = new TriangleTypeDetector(sideA, sideB, sideC);

            string result = detector.DetermineTriangleType();

            Assert.AreEqual("Тупоугольный треугольник", result);
        }

        [TestMethod]
        public void DetermineTriangleType_AcuteTriangle_ReturnsAcuteTriangle()
        {
            double sideA = 8;
            double sideB = 5;
            double sideC = 9;
            TriangleTypeDetector detector = new TriangleTypeDetector(sideA, sideB, sideC);

            string result = detector.DetermineTriangleType();

            Assert.AreEqual("Остроугольный треугольник", result);
        }


        [TestMethod]
        public void DetermineTriangleType_InvalidTriangle_ReturnsInvalidTriangle()
        {
            double sideA = 3;
            double sideB = 300;
            double sideC = 3;
            TriangleTypeDetector detector = new TriangleTypeDetector(sideA, sideB, sideC);

            string result = detector.DetermineTriangleType();

            Assert.AreEqual("Неверные значения сторон", result);
        }
    }
}
